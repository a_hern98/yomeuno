function getMetros(km) {
    return parseInt(km * 100) + " m";
}

function listarPlanes(distance, filtro) {
    $("#planes").empty();
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            let lat = position.coords.latitude;
            let lng = position.coords.longitude;
            $.ajax({
                url: "./conexionesdb/getPlanes.php",
                type: "get", //send it through get method
                data: {
                    lat: lat,
                    lng: lng,
                    distance: distance,
                    filtro: filtro
                },
                success: function (resp) {
                    if (resp !== "No se han encontrado planes") {
                        var planes = JSON.parse(resp);
                        for (let i = 0; i < planes.length; i++) {
                            let curl = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + planes[i].lng + "," + planes[i].lat + ".json?access_token=pk.eyJ1IjoiYWhlcm45OCIsImEiOiJjanV3eGNveWgwZnQ1NGRyeDdyZm5wbXM5In0.TyQcps4fwGFvW0vlFDpuRg";
                            var donde;
                            $.get(curl, function (data) {
                                donde = data.features[0].place_name;
                                var distance;
                                if (planes[i].distance < 1) {
                                    distance = getMetros(planes[i].distance);
                                } else {
                                    distance = parseFloat(planes[i].distance).toFixed(2) + " km";
                                }
                                $("#planes").append(" <div class='row plan' id ='" + planes[i].id + "'>" +
                                    "<h3 class='col s12 m12 l12 center-align'>" + planes[i].titulo + "</h3>" +
                                    "<h5 class='center-align col s12 m12 l12'> Categoria: " + planes[i].tipo + "</h5>" +
                                    "<h5 class='center-align col s12 m12 l12'> Lugar: " + donde + " (" + distance + ")</h5>" +
                                    "<h5 class='center-align col s6 m6 l6'>Autor: " + planes[i].autor + "</h5>" +
                                    "<h5 class='center-align col s6 m6 l6'>Participantes: " + planes[i].participantes + "</h5>" +
                                    "<h5 class='center-align col s6 m6 l6'>Inicio: " + planes[i].inicio + "</h5>" +
                                    "<h5 class='center-align col s6 m6 l6'>Fin: " + planes[i].fin + "</h5>" +
                                    "<h5 class='center-align col s12 m12 l12'>" + planes[i].descripcion + "</h5>" +
                                    "<div class='row'>" +
                                    "<div class='col s1 m2 l2'></div>" +
                                    "<div class='info btn col s10 m8 l8'>" +
                                    "<span>+info</span>" +
                                    "</div>" +
                                    "<div class='col s1 m2 l2'></div>" +
                                    " </div>" +
                                    "</div>");
                                if (i === planes.length - 1) {
                                    $("div.info").click(function () {
                                        let id = $(this).parent().parent().attr("id");
                                        sessionStorage.setItem('id', id);
                                        window.open("infoPlan.html");
                                    })
                                }
                            });
                        }
                    } else {
                        $("#planes").append("<h3 class='col s12 m12 l12 center-align valign-wrapper'> No se han encontrado planes. cambia los filtros en la parte de arriba</h3>");
                    }
                }
            });
        });
    } else {
        $("#planes").append("<h3 class='col s12 m12 l12 center-align valign-wrapper'>Tu navegador no soporta la geolocalizacion</h3>");
    }
}
$(document).ready(function () {
    $("select").formSelect();
    listarPlanes(null, null)
    $('select').change(function () {
        if ($("#filtro").val() == "") {
            var filtro = null;
        } else {
            var filtro = $("#filtro").val()
        }
        if ($("#distance").val() == "") {
            var distance = null;
        } else {
            var distance = $("#distance").val()
        }
        listarPlanes(distance, filtro);
    });
    $("#push").click(function () {
        $('html,body').animate({
            scrollTop: $(".container").offset().top
        }, 1000);
    })

});