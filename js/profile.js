$(document).ready(function () {
  $.get("./conexionesdb/getProfile.php", function (data, status) {
    let user = JSON.parse(data);
    if (user.img !== "") {
      $("#img").attr("src", user.img);
    }
    $("#userName").text(user.nick.toUpperCase());
    $("#location").text(user.city.toUpperCase()+","+user.country);
    $("#age").text("edad " + user.age);
    $("#name").text(user.name.toUpperCase() + " " + user.apellido1.toUpperCase());
    $("#presentacion").text(user.presentacion)
  });
  $('#foto').change(function () {
    $('form').submit()
  });
  $('#foto').change(function (e) {
    var inputFileImage = document.getElementById("foto");
    var file = inputFileImage.files[0];
    var data = new FormData();
    data.append('photo', file);
    $.ajax({
      url: './conexionesdb/uploadImage.php', //URL destino
      data: data,
      type: 'POST',
      processData: false,
      contentType: false,
      success: function (resultado) {
        if (resultado != "update successfully") {
          alert("La imagen no se ha podido actualizar");
          e.stopPropagation();
        }else{
          $(location).attr('href', "profile.html");
        }
      }
    });
  });
  $.ajax({
    url: "./conexionesdb/planesOfUser.php",
    type: "get", //send it through get method
    success: function (resp) {
      if (resp !== "No se han encontrado planes") {
        var planes = JSON.parse(resp);
        for (let i = 0; i < planes.length; i++) {
          let curl = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + planes[i].lng + "," + planes[i].lat + ".json?access_token=pk.eyJ1IjoiYWhlcm45OCIsImEiOiJjanV3eGNveWgwZnQ1NGRyeDdyZm5wbXM5In0.TyQcps4fwGFvW0vlFDpuRg";
          var donde;
          $.get(curl, function (data) {
            donde = data.features[0].place_name;
            $("#planes").append(" <div class='row plan' id ='" + planes[i].id + "'>" +
              "<h3 class='col s12 m12 l12 center-align'>" + planes[i].titulo + "</h3>" +
              "<h5 class='center-align col s12 m12 l12'> Lugar: " + donde +"</h5>" +
              "<h5 class='center-align col s12 m12 l12'>" + planes[i].inicio + " - " + planes[i].fin + "</h5>" +
              "<div class='row'>" +
              "<div class='col s1 m2 l2'></div>" +
              "<div class='info btn col s10 m8 l8'>" +
              "<span>+info</span>" +
              "</div>" +
              "<div class='col s1 m2 l2'></div>" +
              " </div>" +
              "</div>");
            if (i === planes.length - 1) {
              $("div.info").click(function () {
                let id = $(this).parent().parent().attr("id");
                $.ajax({
                  url: "./conexionesdb/getPlan.php",
                  type: "get", //send it through get method
                  data: {
                    id: id
                  },
                  success: function (data) {
                    let plan = JSON.parse(data);
                    let pos = new Array(plan.lng, plan.lat)
                    sessionStorage.setItem('pos', JSON.stringify(pos));
                    sessionStorage.setItem('id', planes[i].id);
                    window.open("infoPlan.html");
                  }
                });
              })
            }
          });
        }
      } else {
        $("#planes").append("<h3 class='col s12 m12 l12 center-align valign-wrapper'>No tienes Planes activos</h3>");
      }
    }
  });
});
