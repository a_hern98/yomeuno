$(document).ready(function () {
  $.get("./isLogged.php", function (data, status) {
    if (data == "false") {
      $(location).attr('href', 'startyomeuno.html');
    }
  });
  $('.sidenav').sidenav();
  $(".dropdown-trigger").dropdown();
  $(".logout").click(function () {
    Cookies.remove('user');
    $(location).attr('href', 'startyomeuno.html');
  });
  $.get("./conexionesdb/getProfile.php", function (data, status) {
    let user = JSON.parse(data);
    if (user.img!=="") {
      $("#imgSideNav").attr("src", user.img);
      $("#imgNav").attr("src", user.img);
    } else {
      $("#imgSideNav").attr("src", "./img_profile/sin_foto.jpg");
      $("#imgNav").attr("src", "./img_profile/sin_foto.jpg");
    }
    $("#usuarioNav").text(user.nick.toUpperCase());
    $("#userName_nav").text(user.nick.toUpperCase());
    $("#name_nav").text(user.name.toUpperCase() + " " + user.apellido1.toUpperCase() + " " + user.apellido2.toUpperCase());
    if (user.img !== null) {
      $("#imgSideNav").attr("src", user.img);
      $("#imgNav").attr("src", user.img);
    } 
  });  
});  