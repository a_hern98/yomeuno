$(document).ready(function () {
  function enviarDatos(id) {
    $(id).submit();
  }
  $('#presentacion').characterCounter();
  $.get("./conexionesdb/getProfile.php", function (data) {
    let user = JSON.parse(data);
    $("#nick").val(user.nick);
    $("#nombre").val(user.name);
    $("#apellido1").val(user.apellido1);
    $("#apellido2").val(user.apellido2);
    $("#presentacion").val(user.presentacion);
    $("#correo").val(user.email).prop('type', 'email');
    $("#country option[value=" + user.contry + "]").attr("selected", true);
    $('#country').formSelect();
    $('#presentacion').characterCounter();
    M.updateTextFields();
    $.ajax({
      url: "./conexionesdb/getCities.php",
      type: "get", //send it through get method
      data: {
        country: $("select#country").val()
      },
      success: function (resp) {
        let cities=JSON.parse(resp);
        for (var i = 0; i < cities.length; i++) {
          $("#city").append('<option value="' + cities[i] + '">' + cities[i] + '</option>');
        }
        $("#city option[value='" + user.city + "']").attr("selected", true);
        $('select#city').formSelect();
      }
    });
  });
  $("#info").submit(function (e) {
    e.preventDefault();
    $.ajax({
      url: "./conexionesdb/update_user.php",
      type: "post",
      data: {
        nick: $("#nick").val(),
        email: $("#correo").val(),
        name:  $("#nombre").val(),
        surname1:  $("#apellido1").val(),
        surname2: $("#apellido2").val(),
        country: $("#country").val(),
        city: $("#city").val(),
        presentacion: $("#presentacion").val()
      },
      success: function (resp) {
        if (resp === "update successfully"){
          Cookies.set('user', $("#nick").val());
          alert("usuario actualizado");
        }
      }
    });
  });
  $("#info").on("keypress", function (e) {
    if (e.which == 13) {
      enviarDatos("#info");
    }
  });
  $("#passwordChange").on("keypress", function (e) {
    if (e.which == 13) {
      enviarDatos("#passwordChange");
    }
  });
  $("#country").change(function () {
    if ($(this).val() != "") {
      $.ajax({
        url: "./conexionesdb/getCities.php",
        type: "get", //send it through get method
        data: {
          country: $("#country").val()
        },
        success: function (resp) {
          $("#city>option").remove();
          let cities=JSON.parse(resp);
          $("#city").html('<option value="" disabled selected>Ciudad...</option>')
          for (var i = 0; i < cities.length; i++) {
            $("#city").append('<option value="' + cities[i] + '">' + cities[i] + '</option>');
          }
           $('select#city').formSelect();
        }
      });
    }
  });

  //CAMBIAR LA CONTRASEÑA
  $('passwdcheck').keyup(function () {
    if ($("#password").val() != $("#passwdcheck").val()) {
      $(".not-match").empty()
      $(".not-match").append('<p> Las contraseñas no coinciden</p>').css("display", 'flex');
    } else {
      $(".not-match").empty()
    }
  });
  $('#passwordChange').submit(function (e) {
    e.preventDefault();
    $('.error').css("display", 'none').empty();
    if ($("#password").val() == $("#passwdcheck").val()) {
      $.ajax({
        url: "./conexionesdb/update_user.php",
        type: "post",
        data: {
          password: $("#password").val()
        },
        success: function (resp) {
          if (resp == "update successfully") {
            $('#message').html('<h4 class="center-align green-text">Contraseña cambiadada</h4>').css("display", 'flex');
          } else {
            $('#message').html('<h4 class="center-align">' + resp + '</h4>').css("display", 'flex');
            e.stopPropagation();
          }
        }
      });
    } else {
      $("#passwdcheck").val("");
      $("#password").val("");
      $("#passwdcheck").css("border-bottom", "4px solid red");
      $("#password").css("border-bottom", "4px solid red");
      e.stopPropagation();
    }
  });
  var myInput = document.getElementById("password");
  var letter = document.getElementById("letter");
  var capital = document.getElementById("capital");
  var number = document.getElementById("number");
  var length = document.getElementById("length");

  // When the user clicks on the password field, show the message box
  myInput.onfocus = function () {
    $("#message").slideDown();
  }

  // When the user clicks outside of the password field, hide the message box
  myInput.onblur = function () {
    $("#message").slideUp();
  }

  // When the user starts to type something inside the password field
  myInput.onkeyup = function () {
    // Validate lowercase letters
    var lowerCaseLetters = /[a-z]/g;
    if (myInput.value.match(lowerCaseLetters)) {
      letter.classList.remove("invalid");
      letter.classList.add("valid");
    } else {
      letter.classList.remove("valid");
      letter.classList.add("invalid");
    }

    // Validate capital letters
    var upperCaseLetters = /[A-Z]/g;
    if (myInput.value.match(upperCaseLetters)) {
      capital.classList.remove("invalid");
      capital.classList.add("valid");
    } else {
      capital.classList.remove("valid");
      capital.classList.add("invalid");
    }

    // Validate numbers
    var numbers = /[0-9]/g;
    if (myInput.value.match(numbers)) {
      number.classList.remove("invalid");
      number.classList.add("valid");
    } else {
      number.classList.remove("valid");
      number.classList.add("invalid");
    }

    // Validate length
    if (myInput.value.length >= 8) {
      length.classList.remove("invalid");
      length.classList.add("valid");
    } else {
      length.classList.remove("valid");
      length.classList.add("invalid");
    }
  }

  $('.modal').modal();
  $("#confirm").click(function (e) {
    e.preventDefault();
      $.ajax({
        url: "./conexionesdb/deleteProfile.php",
        type: "get", //send it through get method
        success: function (resp) {
          Cookies.remove('user');
          $(location).attr('href', 'startyomeuno.html');
        }
      });
  });

});
