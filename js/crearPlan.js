$(document).ready(function () {
  sessionStorage.clear();
  $('#descripcion').characterCounter();

  function enviarDatos() {
    $("form").submit();
  }
  $('select').formSelect();
  $(document).on("keypress", function (e) {
    if (e.which == 13) {
      enviarDatos();
    }
  });
  $("form").submit(function (e) {
    e.preventDefault();
    var privacidad;
    if ($("#isPrivado").is(":checked")) {
      privacidad = 1;
    } else {
      privacidad = 0;
    }
    if ($("#timeEnd").val() == "") {
      $("#timeEnd").val('00:00');
    }
    $.post('./conexionesdb/insertPlan.php', {
        titulo: $("#tituloPlan").val(),
        inicio: $("#dateStar").val() + " " + $("#timeStar").val() + ":00",
        fin: $("#dateEnd").val() + " " + $("#timeEnd").val() + ":00",
        lng: $("#lng").val(),
        lat: $("#lat").val(),
        descripcion: $("#descripcion").val(),
        categoria: $("#tipo").val(),
        privado: privacidad
      },
      function (data) {
        if (data !== "Error") {
          sessionStorage.setItem('id', data);
          $(location).attr('href', "infoPlan.html");
        }
      });
  });
  $('#dateStar').datepicker({
    i18n: {
      months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
      weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
    },
    autoClose: true,
    selectMonths: true,
    yearRange: 0,
    minDate: new Date(),
    labelMonthNext: 'Siguiente mes',
    labelMonthPrev: 'Mes anterior',
    labelMonthSelect: 'Selecciona un mes',
    labelYearSelect: 'Selecciona un año',
    format: 'dd-mmm-yyyy'
  });
  $("#dateStar, #timeStar, #timeEnd, #dateEnd").change(function () {
    if ($("#dateEnd").val() != "" && $("#dateStar").val() != "" && $("#timeEnd").val() != "" && $("#timeStar").val() != "") {
      if (Date.parse($("#dateStar").val() + " " + $("#timeStar").val() + ":00") > Date.parse($("#dateEnd").val() + " " + $("#timeEnd").val() + ":00")) {
        alert("la fecha de inicio del plan no puede ser posterior a la de finalizacion")
        $("#dateEnd").val("")
        $("#timeEnd").val("")
      }
    }
  });
  $("#dateStar").change(function () {
    if ($("#dateStar").val() != "") {
      $('#dateEnd').datepicker({
        i18n: {
          months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
          weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
          weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
        },
        autoClose: true,
        selectMonths: true,
        yearRange: 0,
        minDate: new Date($("#dateStar").val()),
        labelMonthNext: 'Siguiente mes',
        labelMonthPrev: 'Mes anterior',
        labelMonthSelect: 'Selecciona un mes',
        labelYearSelect: 'Selecciona un año',
        format: 'dd-mmm-yyyy'
      });
    }
  });
  $('.timepicker').timepicker({
    twelveHour: false,
    autoClose: true
  });
});