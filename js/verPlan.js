function getParameterByURL(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(document).ready(function () {
    if (window.location.pathname !== "/yomeuno/infoPlan.html") {
        sessionStorage.setItem("id", getParameterByURL("id"));
        sessionStorage.setItem('pos', JSON.stringify(new Array(getParameterByURL("lng"), getParameterByURL("lng"))));
    }
    if (sessionStorage.getItem("id") !== 'undefined' && sessionStorage.getItem("pos") !== 'undefined') {
        let id = sessionStorage.getItem("id");
        $.ajax({
            url: "./conexionesdb/getPlan.php",
            type: "get", //send it through get method
            data: {
                id: id
            },
            success: function (resp) {
                var plan = JSON.parse(resp);
                let curl = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + plan.lng + "," + plan.lat + ".json?access_token=pk.eyJ1IjoiYWhlcm45OCIsImEiOiJjanV3eGNveWgwZnQ1NGRyeDdyZm5wbXM5In0.TyQcps4fwGFvW0vlFDpuRg";
                var donde;
                $.get(curl, function (data) {
                    donde = data.features[0].place_name;
                    $("#name").text(plan.titulo)
                    $("#params").append("<h5 class='center-align col s12 m12 l6'> Categoria: " + plan.tipo + "</h5>" +
                        "<h5 class='center-align col s12 m12 l6'>Autor: " + plan.autor + "</h5>" +
                        "<h5 class='center-align col s12 m12 l6'>Inicio: " + plan.inicio + "</h5>" +
                        "<h5 class='center-align col s12 m12 l6'>Fin: " + plan.fin + "</h5>" +
                        "<h5 class='center-align col s12 m12 l12'> " + plan.descripcion + "</h5>" +
                        "<h5 class='center-align col s12 m12 l12'> Lugar: " + donde + "</h5>");

                });
                plan[0].forEach(participante => {
                    $("#participantes").append(' <div class="col s10 m10 offset-m2 l10 offset-l3">' +
                        '<div class="card-panel grey lighten-5 z-depth-1 participante">' +
                        ' <div class="row valign-wrapper">' +
                        ' <div class="col s4 m4 l4">' +
                        ' <img src=' + participante.foto + ' alt="" class="circle responsive-img"> ' +
                        '</div>' +
                        '<div class="col s10">' +
                        '  <span class="black-text">' +
                        participante.nick.toUpperCase() +
                        '  </span>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                    if (participante.nick == Cookies.get('user')) {
                        $("#puerta>span").text("YO ME SALGO");
                    }
                });
                let coordenadas = new Array(plan.lng, plan.lat);
                mapboxgl.accessToken = 'pk.eyJ1IjoiYWhlcm45OCIsImEiOiJjanV3eGNveWgwZnQ1NGRyeDdyZm5wbXM5In0.TyQcps4fwGFvW0vlFDpuRg';

                let map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v9',
                    center: coordenadas,
                    zoom: 15
                });
                let marker = new mapboxgl.Marker({
                        draggable: false
                    })
                    .setLngLat(coordenadas);
                map.addControl(new mapboxgl.FullscreenControl());
                marker.addTo(map);
            }
        });
        $("#puerta").click(function () {
            let url;
            if ($("#puerta>span").text() == "YO ME SALGO") {
                url = "./conexionesdb/salirPlan.php"
            } else {
                url = "./conexionesdb/unirsePlan.php"
            }

            $.post(url, {
                plan: sessionStorage.getItem("id")
            }, function (act) {
                if (act == "se ha borrado el plan") {
                    alert("el plan se ha borrado por falta de miembros");
                    window.close();
                } else {
                    $(location).attr('href', "infoPlan.html");
                };
            })
        })
        $("#edit").click(function () {
            $(location).attr('href', "editPlan.html");
        })
        $.get('./conexionesdb/usuarioTienePrivilegios.php', {
            plan: sessionStorage.getItem("id")
        }, function (data) {
            if (data == "S") {
                $("#edit").append('<span>EDITAR PLAN</span>');
                $("#edit").attr('class', 'btn col s5 m4 l4');
                $("#puerta").attr('class', 'btn col s5 m4 l4')
            }
        })
    } else {
        $(location).attr('href', "navbar.html");
    }

});