$(document).ready(function() {
  $.get('./conexionesdb/usuarioTienePrivilegios.php', {
    plan: sessionStorage.getItem("id")
  }, function(data) {
    if (data == "N") {
      alert("No puedes editar este plan")
      $(location).attr('href', "navbar.html");
    }
  })
  $('#descripcion').characterCounter();

  function enviarDatos() {
    $("form").submit();
  }
  $('select').formSelect();
  $(document).on("keypress", function(e) {
    if (e.which == 13) {
      enviarDatos();
    }
  });
  let id = sessionStorage.getItem("id");
  $.ajax({
    url: "./conexionesdb/getPlan.php",
    type: "get", //send it through get method
    data: {
      id: id,
    },
    success: function(data) {
      let plan = JSON.parse(data);
      $("#tituloPlan").val(plan.titulo);
      let datatime;
      datatime = plan.inicio.split(" ");
      $("#dateStar").val(datatime[0]);
      time = datatime[1].split(":");
      $("#timeStar").val(time[0] + ":" + time[1]);
      datatime = plan.fin.split(" ");
      $("#dateEnd").val(datatime[0]);
      time = datatime[1].split(":");
      $("#timeEnd").val(time[0] + ":" + time[1]);
      $("#lng").val(plan.lng);
      $("#lat").val(plan.lat);
      $("#descripcion").val(plan.descripcion);
      $("#tipo option[value=" + plan.tipo + "]").attr("selected", true);
      $('select').formSelect();
      M.updateTextFields();
      if (plan.privacidad == "0") {
        $('#isPrivado').prop('checked', false);
      } else {
        $('#isPrivado').prop('checked', true);
      }
      $("#enunciado").text($("#enunciado").text() + " #" + plan.id)
      let pos = new Array(plan.lng, plan.lat)
      sessionStorage.setItem('pos', JSON.stringify(pos));
      $("form").submit(function(e) {
        e.preventDefault();
        var privacidad;
        if ($("#isPrivado").is(":checked")) {
          privacidad = 1;
        } else {
          privacidad = 0;
        }
        if ($("#timeEnd").val() == "") {
          $("#timeEnd").val('00:00');
        }
        $.post('./conexionesdb/updatePlan.php', {
            id: id,
            titulo: $("#tituloPlan").val(),
            inicio: $("#dateStar").val() + " " + $("#timeStar").val() + ":00",
            fin: $("#dateEnd").val() + " " + $("#timeEnd").val() + ":00",
            lng: $("#lng").val(),
            lat: $("#lat").val(),
            descripcion: $("#descripcion").val(),
            categoria: $("#tipo").val(),
            privado: privacidad
          },
          function(data) {
            if (data !== "Error") {
              let pos = new Array($("#lng").val(), $("#lat").val())
              sessionStorage.setItem('pos', JSON.stringify(pos));
              $(location).attr('href', "infoPlan.html");
            }
          });
      });
      $('#dateEnd').datepicker({
        i18n: {
          months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
          weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
          weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
        },
        autoClose: true,
        selectMonths: true,
        yearRange: 0,
        minDate: new Date($("#dateStar").val()),
        labelMonthNext: 'Siguiente mes',
        labelMonthPrev: 'Mes anterior',
        labelMonthSelect: 'Selecciona un mes',
        labelYearSelect: 'Selecciona un año',
        format: 'dd-mmm-yyyy'
      });
    }
  });
  $('#dateStar').datepicker({
    i18n: {
      months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
      weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
    },
    autoClose: true,
    selectMonths: true,
    yearRange: 0,
    minDate: new Date(),
    labelMonthNext: 'Siguiente mes',
    labelMonthPrev: 'Mes anterior',
    labelMonthSelect: 'Selecciona un mes',
    labelYearSelect: 'Selecciona un año',
    format: 'dd-mmm-yyyy'
  });
  $("#dateStar").change(function() {
    if ($("#dateStar").val() != "") {
      $('#dateEnd').datepicker({
        i18n: {
          months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
          weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
          weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
        },
        autoClose: true,
        selectMonths: true,
        yearRange: 0,
        minDate: new Date($("#dateStar").val()),
        labelMonthNext: 'Siguiente mes',
        labelMonthPrev: 'Mes anterior',
        labelMonthSelect: 'Selecciona un mes',
        labelYearSelect: 'Selecciona un año',
        format: 'dd-mmm-yyyy'
      });
    }
  });
  $('.timepicker').timepicker({
    twelveHour: false,
    autoClose: true
  });
  $("#dateStar, #timeStar, #timeEnd, #dateEnd").change(function() {
    if ($("#dateEnd").val() != "" && $("#dateStar").val() != "" && $("#timeEnd").val() != "" && $("#timeStar").val() != "") {
      if (Date.parse($("#dateStar").val() + " " + $("#timeStar").val() + ":00") > Date.parse($("#dateEnd").val() + " " + $("#timeEnd").val() + ":00")) {
        alert("la fecha de inicio del plan no puede ser posterior a la de finalizacion")
        $("#dateEnd").val("")
        $("#timeEnd").val("")
      }
    }
  });
});
