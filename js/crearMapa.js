// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.

// function initMap() {
//   var map = new google.maps.Map(document.getElementById('map'), {
//     center: {
//       lat: -34.397,
//       lng: 150.644
//     },
//     zoom: 15
//   });
//   var infoWindow = new google.maps.InfoWindow;
//
//   // Try HTML5 geolocation.
//   if (navigator.geolocation) {
//     navigator.geolocation.getCurrentPosition(function(position) {
//       var pos = {
//         lat: position.coords.latitude,
//         lng: position.coords.longitude
//       };
//       map.setCenter(pos);
//     }, function() {
//       handleLocationError(true, infoWindow, map.getCenter());
//     });
//   } else {
//     // Browser doesn't support Geolocation
//     handleLocationError(false, infoWindow, map.getCenter());
//   }
// }
//
// function handleLocationError(browserHasGeolocation, infoWindow, pos) {
//   infoWindow.setPosition(pos);
//   infoWindow.setContent(browserHasGeolocation ?
//     'Error: The Geolocation service failed.' :
//     'Error: Your browser doesn\'t support geolocation.');
//   infoWindow.open(map);
// }
function initMap(coordenadas) {
  let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: coordenadas,
    zoom: 15
  });
  return map;
}
function initMarker(coordenadas) {
  let marker = new mapboxgl.Marker({
    draggable: true
  })
    .setLngLat(coordenadas);
  return marker;
}
mapboxgl.accessToken = 'pk.eyJ1IjoiYWhlcm45OCIsImEiOiJjanV3eGNveWgwZnQ1NGRyeDdyZm5wbXM5In0.TyQcps4fwGFvW0vlFDpuRg';
var map;
var marker;
if (sessionStorage.getItem("pos")) {
  var pos = JSON.parse(sessionStorage.getItem("pos"));
}
if (typeof pos === 'undefined') {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      const pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      }
      map = initMap(pos);
      map.addControl(new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
      }));
      marker = initMarker(pos);
      completarMap(pos);
    });
  } else {
    map = initMap([0, 0]);
    marker = initMarker([0, 0]);
    completarMap(pos);
  }
} else {
  map = initMap(pos);
  if (navigator.geolocation) {
    map.addControl(new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      trackUserLocation: true
    }));
  }
  marker = initMarker(pos);
  completarMap(pos);
}
function completarMap(pos) {
  map.addControl(new mapboxgl.NavigationControl());
  map.addControl(new mapboxgl.FullscreenControl());
  marker.addTo(map);
  $("#lng").val(pos.lng);
  $("#lat").val(pos.lat);
  function onDragEnd() {
    var lngLat = marker.getLngLat();
    $("#lng").val(lngLat.lng);
    $("#lat").val(lngLat.lat);
  }
  marker.on('dragend', onDragEnd);
}