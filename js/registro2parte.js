$(document).ready(function () {
  $.get("./isLogged.php", function (data, status) {
    if (data == "false") {
      $(location).attr('href', 'startyomeuno.html');
    }
  });
  $('#presentacion').characterCounter();
  $('#finishRegister').click(function () {
    $('#formulario').submit();
  });
  $(document).on('keypress', function (e) {
    if (e.which == 13) {
      $('#formulario').submit();
    }
  });
  $('#formulario').submit(function (e) {
    e.preventDefault();
    if ($("#foto").val() !== "") {
      var inputFileImage = document.getElementById("foto");
      var file = inputFileImage.files[0];
      var data = new FormData();
      data.append('photo', file);
      $.ajax({
        url: './conexionesdb/uploadImage.php', //URL destino
        data: data,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function (resultado) {
          if (data != "update successfully") {
            e.stopPropagation();
          }
        }
      });
    }
    if ($("#presentacion").val() !== "") {
      $.post('./conexionesdb/update_user.php', {
        'presentacion': $("#presentacion").val()
      },
        function (data, status) {
          if (data != "update successfully") {
            e.stopPropagation();
          }
        });
    }
    $(location).attr('href', 'navbar.html')
  });
});
