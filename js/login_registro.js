$(document).ready(function() {
  $.get("./isLogged.php", function(data, status){
    if (data == "true") {
      $(location).attr('href', 'navbar.html');
    } 
  });
  $('select#country').formSelect();
  $('.change').click(function() {
    $('.formulario').animate({
      height: "toggle",
      'padding-top': 'toggle',
      'padding-bottom': 'toggle',
      opacity: 'toggle'
    }, "slow");
  });
  //login
  $('#login').submit(function(e) {
    e.preventDefault();
    $("#errorL").empty();
    $.post('./conexionesdb/login.php', {
        'userid': $("#userid").val(),
        'password': $("#password").val(),
      },
      function(data, status) {
        if (data != "Has introducido mal tus datos") {
          Cookies.set('user', data);
          $(location).attr('href', "navbar.html");
        } else {
          $("#errorL").append("<p>Has introducido mal tus datos</p>").css("display", 'flex');
          e.stopPropagation();
        }
      });
  });
  $('.datepicker').datepicker({
    i18n: {
      months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
      weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
    },
    autoClose: true,
    maxDate: new Date(),
    selectMonths: true,
    yearRange: [(new Date).getFullYear() - 100, (new Date).getFullYear()],
    labelMonthNext: 'Siguiente mes',
    labelMonthPrev: 'Mes anterior',
    labelMonthSelect: 'Selecciona un mes',
    labelYearSelect: 'Selecciona un año',
    format: 'dd-mmm-yyyy'
  });
  $('input').click(function() {
    $('.error').css("display", 'none').empty();
  });
  var scroll = (loc) => {
    $('html,body').animate({
      scrollTop: $("#" + loc).offset().top
    }, 1000);
  }
  $('div.scroll + input:gt(1)').click(function() {
    let loc = $(this).prev().attr('id')
    scroll(loc)
  });
  $('#registrarse > input#passwdcheck').keyup(function() {
    if ($("#passwordregister").val() != $("#passwdcheck").val()) {
      $(".not-match").empty()
      $(".not-match").append('<p> Las contraseñas no coinciden</p>').css("display", 'flex');
    } else {
      $(".not-match").empty()
    }
  });
  //registro
  $('#registrarse').submit(function(e) {
    e.preventDefault();
    $('.error').css("display", 'none').empty();
    if ($("#passwordregister").val() == $("#passwdcheck").val()) {
      $.post('./conexionesdb/registro.php', {
          'nick': $("#username").val(),
          'password': $("#passwordregister").val(),
          'email': $("#email").val(),
          'name': $("#name").val(),
          'surname1': $("#surname1").val(),
          'surname2': $("#surname2").val(),
          'country': $("#country").val(),
          'city': $("#city").val(),
          'birth': $("#birth").val(),
        },
        function(data, status) {
          if (data == "El nombre de usuario ya esta dado de alta") {
            $("#errorUsername").append('<p>' + data + '</p>').css("display", 'flex');
            $("#username").css("border-bottom", " 4px solid red");
            scroll($("#username").prev().attr('id'));
            e.stopPropagation();
          } else if (data == "El email ya en uso") {
            $("#errorEmail").append('<p>' + data + '</p>').css("display", 'flex');;
            $("#email").css("border-bottom", "4px solid red");
            scroll($("#email").prev().attr('id'));
            e.stopPropagation();
          } else if (data == "true") {
            Cookies.set('user', $("#username").val());
            $(location).attr('href', "registro2.html");
          }
        });
    } else {
      $("#passwdcheck").val("");
      $("#passwordregister").val("");
      $("#passwdcheck").css("border-bottom", "4px solid red");
      $("#passwordregister").css("border-bottom", "4px solid red");
      scroll($("#passwordregister").prev().attr('id'));
      e.stopPropagation();
    }
  });
  $("#country").change(function () {
    if ($(this).val() != "") {
      $.ajax({
        url: "./conexionesdb/getCities.php",
        type: "get", //send it through get method
        data: {
          country: $("#country").val()
        },
        success: function (resp) { 
          $("#city>option").remove();
          let cities=JSON.parse(resp);
          $("#city").html('<option value="" disabled selected>Ciudad...</option>')
          for (var i = 0; i < cities.length; i++) {
            $("#city").append('<option value="' + cities[i] + '">' + cities[i] + '</option>');
          }
           $('select#city').formSelect();
        }
      });
    }
  });
  var myInput = document.getElementById("passwordregister");
  var letter = document.getElementById("letter");
  var capital = document.getElementById("capital");
  var number = document.getElementById("number");
  var length = document.getElementById("length");

  // When the user clicks on the password field, show the message box
  myInput.onfocus = function() {
    $("#message").slideDown();
  }

  // When the user clicks outside of the password field, hide the message box
  myInput.onblur = function() {
    $("#message").slideUp();
  }

  // When the user starts to type something inside the password field
  myInput.onkeyup = function() {
    // Validate lowercase letters
    var lowerCaseLetters = /[a-z]/g;
    if (myInput.value.match(lowerCaseLetters)) {
      letter.classList.remove("invalid");
      letter.classList.add("valid");
    } else {
      letter.classList.remove("valid");
      letter.classList.add("invalid");
    }

    // Validate capital letters
    var upperCaseLetters = /[A-Z]/g;
    if (myInput.value.match(upperCaseLetters)) {
      capital.classList.remove("invalid");
      capital.classList.add("valid");
    } else {
      capital.classList.remove("valid");
      capital.classList.add("invalid");
    }

    // Validate numbers
    var numbers = /[0-9]/g;
    if (myInput.value.match(numbers)) {
      number.classList.remove("invalid");
      number.classList.add("valid");
    } else {
      number.classList.remove("valid");
      number.classList.add("invalid");
    }

    // Validate length
    if (myInput.value.length >= 8) {
      length.classList.remove("invalid");
      length.classList.add("valid");
    } else {
      length.classList.remove("valid");
      length.classList.add("invalid");
    }
  }
});