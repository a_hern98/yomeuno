<?php
require_once("startconect.php");
$nick = $_COOKIE["user"];
$now=date("Y-m-d H:i:s");
$sql= "SELECT p.id ,p.titulo_plan, p.fecha_de_quedada, p.fecha_de_finalizacion, p.lat, p.lng FROM plan p, plan_usuario pu WHERE p.id=pu.plan AND pu.usuario='$nick' AND p.fecha_de_quedada >= '$now'";
$result= $conn->query($sql);
$planes=array();
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $id=$row["id"];
        $titulo=$row["titulo_plan"];
        $lat=$row["lat"];
        $lng=$row["lng"];
        $inicio = $row["fecha_de_quedada"];
        $fin = $row["fecha_de_finalizacion"];
        $infoPlan = array('id' => $id,'titulo' => $titulo,'lat'=> $lat,'lng' => $lng, 'inicio'=> $inicio, 'fin' => $fin);
        array_push($planes, $infoPlan);
    }
} else {
    echo "No se han encontrado planes";
}
if (!empty($planes)){
    echo json_encode($planes);
}
require_once("endconexion.php");
?>