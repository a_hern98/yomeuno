<?php 
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "gps";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if(isset($_GET["country"])){
    $country=$_GET["country"];
    $sql= "SELECT city FROM cities WHERE country ='$country'";
    //echo $sql;
    $result = $conn->query($sql);
    $cities=array();
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            array_push($cities,$row["city"]);            
        }
        echo json_encode($cities);
    } else {
        echo "0 results";
    }
}else{
    echo "Porfavor seleciona un pais";
}
require_once("endconexion.php");
?>