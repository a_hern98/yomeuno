<?php 
require_once("startconect.php");
if(isset($_GET["distance"]) && !empty($_GET["distance"])){
    $distance=$_GET["distance"];
} else {
    $distance= 50;
}
$lat=$_GET["lat"];
$lng=$_GET["lng"];
$now=date("Y-m-d H:i:s");
if(isset($_GET["filtro"]) && !empty($_GET["filtro"])){
    $filtro=$_GET["filtro"];
    $sql= "SELECT *, ( 6371 * acos(cos(radians('$lat')) * cos(radians(p.lat)) * cos(radians(p.lng) - radians('$lng')) + sin(radians('$lat')) * sin(radians(p.lat)))) AS distance, (SELECT COUNT(*) FROM  plan_usuario pa WHERE p.id=pa.plan) AS participantes FROM plan p WHERE privado=0 AND p.categoria AND p.fecha_de_quedada >= '$now'LIKE '$filtro' HAVING distance <= '$distance' ORDER BY distance";
} else {
    $sql= "SELECT *, ( 6371 * acos(cos(radians('$lat')) * cos(radians(p.lat)) * cos(radians(p.lng) - radians('$lng')) + sin(radians('$lat')) * sin(radians(p.lat)))) AS distance, (SELECT COUNT(*) FROM plan_usuario pa WHERE p.id=pa.plan) AS participantes FROM plan p WHERE privado=0 AND p.fecha_de_quedada >= '$now' HAVING distance <= '$distance' ORDER BY distance";
}
$result= $conn->query($sql);
$planes=array();
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $id=$row["id"];
        $participantes=$row["participantes"];
        $titulo=$row["titulo_plan"];
        $distance=$row["distance"];
        $lat=$row["lat"];
        $lng=$row["lng"];
        $inicio=$row["fecha_de_quedada"];
        $fin=$row["fecha_de_finalizacion"];
        $privacidad=$row["privado"];
        $autor=$row["autor"];
        $descripcion=$row["descripcion"];
        $tipo=$row["categoria"];
        $plan = array('id' => $id,'titulo' => $titulo, 'tipo' => $tipo,'distance'=>$distance,'participantes' => $participantes, 'descripcion' => $descripcion,'lat'=> $lat,'lng' => $lng, 'inicio'=> $inicio, 'fin' => $fin,'privacidad' => $privacidad, 'autor' => $autor);
        array_push($planes, $plan);
    }
} else {
    echo "No se han encontrado planes";
}
if (!empty($planes)){
    echo json_encode($planes);
}
require_once("endconexion.php");
?>