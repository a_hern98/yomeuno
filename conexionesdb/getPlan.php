<?php
require_once("startconect.php");
$id=$_GET["id"];
$plan="SELECT * FROM `plan` WHERE `id` = '$id'";
$result = mysqli_query($conn, $plan);
$infoPlan= array();
if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_assoc($result);
    $id=$row["id"];
    $titulo=$row["titulo_plan"];
    $lat=$row["lat"];
    $lng=$row["lng"];
    $fecha = date_create_from_format('Y-m-j H:i:s', $row["fecha_de_quedada"]);
    $inicio = date_format($fecha,'j-M-Y H:i:s');
    $fecha = date_create_from_format('Y-m-j H:i:s', $row["fecha_de_finalizacion"]);
    $fin = date_format($fecha,'j-M-Y H:i:s');
    $privacidad=$row["privado"];
    $autor=$row["autor"];
    $descripcion=$row["descripcion"];
    $tipo=$row["categoria"];
    $infoPlan = array('id' => $id,'titulo' => $titulo,'tipo' => $tipo, 'descripcion' => $descripcion,'lat'=> $lat,'lng' => $lng, 'inicio'=> $inicio, 'fin' => $fin,'privacidad' => $privacidad, 'autor' => $autor);
}
$participantessql= "SELECT u.nick, u.foto FROM usuario u, plan_usuario pu WHERE pu.plan='$id' AND pu.usuario=u.nick";
$participantes = array();
$result2= $conn->query($participantessql);
if (mysqli_num_rows($result2) > 0) {
    while($row = mysqli_fetch_assoc($result2)) {
        $nick=$row["nick"];
        $img=$row["foto"];   
        $rutaImg=str_replace("\"","",$img);
        $participante= array('nick'=>$nick, 'foto'=>$rutaImg);
        array_push($participantes, $participante);
    }
}
array_push($infoPlan, $participantes);
echo json_encode($infoPlan);
require_once("endconexion.php");
?>